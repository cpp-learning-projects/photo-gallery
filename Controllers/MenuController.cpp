//
// Created by Piotr Rudnicki on 30/03/2024.
//

#include "MenuController.h"
#include <QMenuBar>
#include "MainWindow.h"
#include "../Commands/Menu/OpenFolderCommand.h"

MenuController::MenuController(MainWindow& mainWindow, FileManager& fileManager)
: mainWindow(mainWindow), fileManager(fileManager) {
    menuBar = new QMenuBar(&mainWindow);
    mainWindow.setMenuBar(menuBar);
}

void MenuController::setupMenus() {
    createFileMenu();
}

void MenuController::createFileMenu() {
    QMenu* fileMenu = menuBar->addMenu("File");
    QAction* openFolderAction = fileMenu->addAction("Open Folder");
    OpenFolderCommand* openFolderCommand = new OpenFolderCommand(mainWindow, fileManager);
    QObject::connect(openFolderAction, &QAction::triggered, [openFolderCommand]() {
        openFolderCommand->execute();
    });
}