//
// Created by Piotr Rudnicki on 30/03/2024.
//

#include "MainWindow.h"
#include "MenuController.h"
#include <QLabel>
#include "../Views/ImagesGridView.h"
#include "../Services/ImageLoader.h"
#include "../Services/FileManager.h"

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent)
{
    fileManager = new FileManager();
    setWindowTitle("Image Viewer");
    setMinimumSize(800, 600);

    imageLoader = new ImageLoader(*fileManager);
    connect(imageLoader, &ImageLoader::imagesLoaded, this, &MainWindow::onImagesLoaded);

    imagesGridView = new ImagesGridView(this);
    setCentralWidget(imagesGridView);

    menuController = new MenuController(*this, *fileManager);
    menuController->setupMenus();
}

void MainWindow::onImagesLoaded(const QVector<QImage> &images)
{
    imagesGridView->displayImages(images);
}