//
// Created by Piotr Rudnicki on 30/03/2024.
//

#ifndef PHOTO_GALLERY_MENUCONTROLLER_H
#define PHOTO_GALLERY_MENUCONTROLLER_H

class QMenuBar;
class MainWindow;
class FileManager;

class MenuController {
public:
    MenuController(MainWindow& mainWindow, FileManager& fileManager) ;
    void setupMenus();

private:
    MainWindow& mainWindow;
    QMenuBar* menuBar;
    FileManager& fileManager;

    void createFileMenu();
};


#endif //PHOTO_GALLERY_MENUCONTROLLER_H
