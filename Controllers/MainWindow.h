//
// Created by Piotr Rudnicki on 30/03/2024.
//

#ifndef PHOTO_GALLERY_MAINWINDOW_H
#define PHOTO_GALLERY_MAINWINDOW_H


#include <QMainWindow>

class ImagesGridView;
class ImageLoader;
class FileManager;
class MenuController;

class MainWindow : public QMainWindow{
public:
    explicit MainWindow(QWidget *parent = nullptr);

private:
    ImagesGridView* imagesGridView;
    ImageLoader* imageLoader;
    FileManager* fileManager;
    MenuController* menuController;

    void onImagesLoaded(const QVector<QImage>& images);
};


#endif //PHOTO_GALLERY_MAINWINDOW_H
