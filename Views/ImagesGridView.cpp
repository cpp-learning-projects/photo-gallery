//
// Created by Piotr Rudnicki on 31/03/2024.
//

#include "ImagesGridView.h"
#include <QGridLayout>
#include <QLabel>

ImagesGridView::ImagesGridView(QWidget* parent) : QWidget(parent)
{
    layout = new QGridLayout;
    layout->setSpacing(2);
    setLayout(layout);
}

void ImagesGridView::resizeEvent(QResizeEvent *event)
{
    Q_UNUSED(event);
    recalculateLayout();
}

void ImagesGridView::displayImages(const QVector<QImage>& images)
{
    if(this->images != nullptr && this->images != &images)
    {
        delete this->images;
    }
    this->images = new QVector<QImage>(images);
    int row = 0;
    int col = 0;
    for(const QImage& image : images)
    {
        QLabel* label = new QLabel;
        label->setPixmap(QPixmap::fromImage(image));
        layout->addWidget(label, row, col++);
        if(col>=columns)
        {
            col = 0;
            row++;
        }
    }
}

void ImagesGridView::recalculateLayout()
{
    QLayoutItem* item;
    while((item = layout->takeAt(0)) != nullptr)
    {
        if(item->widget() != nullptr)
        {
            delete item->widget();
        }
        delete item;
    }

    int availableWidth = width();
    columns = std::max(1,availableWidth / columnWidth);
    if(images != nullptr)
    {
        this->displayImages(*images);
    }
}