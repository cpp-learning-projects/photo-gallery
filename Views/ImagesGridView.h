//
// Created by Piotr Rudnicki on 31/03/2024.
//

#ifndef PHOTO_GALLERY_IMAGESGRIDVIEW_H
#define PHOTO_GALLERY_IMAGESGRIDVIEW_H

#include<QVector>
#include <QWidget>

class QImage;
class QGridLayout;

class ImagesGridView : public QWidget
{
private:
    QGridLayout* layout;
    QVector<QImage>* images;

    void recalculateLayout();
    void resizeEvent(QResizeEvent *event) override;
    int columnWidth = 102;
    int columns;
public:
    explicit ImagesGridView(QWidget* parent = nullptr);
    void displayImages(const QVector<QImage>& images);
};


#endif //PHOTO_GALLERY_IMAGESGRIDVIEW_H
