//
// Created by Piotr Rudnicki on 30/03/2024.
//

#include <QString>
#include <QFileDialog>
#include "OpenFolderCommand.h"
#include "../../Services/FileManager.h"

void OpenFolderCommand::execute()
{
    QString path = QFileDialog::getExistingDirectory(nullptr, "Select a folder", QDir::homePath());
    if (!path.isEmpty())
    {
        fileManager.openFolder(path);
    }
}