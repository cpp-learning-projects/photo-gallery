//
// Created by Piotr Rudnicki on 30/03/2024.
//

#ifndef PHOTO_GALLERY_OPENFOLDERCOMMAND_H
#define PHOTO_GALLERY_OPENFOLDERCOMMAND_H

class QWidget;
class FileManager;

#include "../Command.h"

class OpenFolderCommand : public Command{
private:
    QWidget& widget;
    FileManager& fileManager;
public:
    OpenFolderCommand(QWidget& widget, FileManager& fileManager) : widget(widget), fileManager(fileManager) {}
    void execute() override;
};


#endif //PHOTO_GALLERY_OPENFOLDERCOMMAND_H
