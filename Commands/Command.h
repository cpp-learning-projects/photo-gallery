//
// Created by Piotr Rudnicki on 30/03/2024.
//

#ifndef PHOTO_GALLERY_COMMAND_H
#define PHOTO_GALLERY_COMMAND_H

class Command
{
public:
    virtual ~Command() = default;
    virtual void execute() = 0;
};

#endif //PHOTO_GALLERY_COMMAND_H
