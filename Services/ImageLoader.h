//
// Created by Piotr Rudnicki on 30/03/2024.
//

#ifndef PHOTO_GALLERY_IMAGELOADER_H
#define PHOTO_GALLERY_IMAGELOADER_H

#include <QObject>
#include <QSize>
#include <QImage>
#include <QFutureWatcher>

class FileManager;
class ImageLoader : public QObject
{
    Q_OBJECT
private:
    QSize const thumbnailSize = QSize(100, 100);
    QFutureWatcher<QVector<QImage>> watcher;
    QVector<QImage> loadImages(const QStringList& paths);

public:
    explicit ImageLoader(FileManager& fileManager, QObject *parent = nullptr);

signals:
    void imagesLoaded(const QVector<QImage>& images);

private slots:
    void loadImagesAsync(const QStringList& paths);
    void handleImagesLoaded();
};


#endif //PHOTO_GALLERY_IMAGELOADER_H
