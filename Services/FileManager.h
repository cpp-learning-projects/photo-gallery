//
// Created by Piotr Rudnicki on 30/03/2024.
//

#ifndef PHOTO_GALLERY_FILEMANAGER_H
#define PHOTO_GALLERY_FILEMANAGER_H

#include <QObject>
class QString;

class FileManager : public QObject
{
Q_OBJECT
public:
    void openFolder(QString& path);

signals:
    void folderOpened(QStringList& imageFiles);
};


#endif //PHOTO_GALLERY_FILEMANAGER_H
