//
// Created by Piotr Rudnicki on 30/03/2024.
//

#include <QDir>
#include <QImage>
#include <QtConcurrent/QtConcurrent>
#include "ImageLoader.h"
#include "FileManager.h"

ImageLoader::ImageLoader(FileManager& fileManager, QObject *parent) : QObject(parent)
{
    connect(&fileManager, &FileManager::folderOpened, this, &ImageLoader::loadImagesAsync);
    connect(&watcher, &QFutureWatcher<QVector<QImage>>::finished, this, &ImageLoader::handleImagesLoaded);
}

void ImageLoader::loadImagesAsync(const QStringList &paths)
{
    QFuture<QVector<QImage>> future = QtConcurrent::run([this,paths](){
        return this->loadImages(paths);
    });
    watcher.setFuture(future);
}

QVector<QImage> ImageLoader::loadImages(const QStringList &paths)
{
    QVector<QImage> images;

    for (const QString& filePath : paths)
    {
        QImage image(filePath);
        if(!image.isNull())
        {
            images.push_back(image.scaled(thumbnailSize, Qt::KeepAspectRatio, Qt::SmoothTransformation));
        }
    }
    return images;
}

void ImageLoader::handleImagesLoaded()
{
    emit imagesLoaded(watcher.result());
}