//
// Created by Piotr Rudnicki on 30/03/2024.
//

#include <QDir>
#include "FileManager.h"

void FileManager::openFolder(QString& path)
{
    QDir dir(path);
    dir.makeAbsolute();
    QStringList imageFiles = dir.entryList(QStringList() << "*.jpg" << "*.jpeg" << "*.png", QDir::Files);
    QStringList absolutePaths;
    for(const QString& fileName:imageFiles)
    {
        QString absolutePath = dir.absoluteFilePath(fileName);
        absolutePaths.push_back(absolutePath);
    }
    emit folderOpened(absolutePaths);
}